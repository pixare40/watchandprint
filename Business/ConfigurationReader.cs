﻿using BusinessLayer.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BusinessLayer
{
    public class ConfigurationReader : IConfigurationReader
    {
        public ConfigurationModel ConfigurationModel { get; set; }

        public ConfigurationReader()
        {
            string configurationPath = @"C:\config\configuration.json";
            if (!File.Exists(configurationPath))
            {
                Console.WriteLine("Error, No configuration file");
                return;
            }

            string jsonConfig = File.ReadAllText(configurationPath);
            ConfigurationModel = JsonConvert.DeserializeObject<ConfigurationModel>(jsonConfig);
        }

        public string GetWatchFolder()
        {
            return ConfigurationModel.WatchFolder;
        }

        public string GetPrintFolder()
        {
            return ConfigurationModel.PrintFolder;
        }
    }
}
