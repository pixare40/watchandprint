﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class ConfigurationModel
    {
        public string WatchFolder { get; set; }

        public string PrintFolder { get; set; }

        public int Margin { get; set; }

        public string FontFamily { get; set; }

        public int FontSize { get; set; }
    }
}
