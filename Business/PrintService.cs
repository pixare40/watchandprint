﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace BusinessLayer
{
    public class PrintService : IPrintService
    {
        private IConfigurationReader configurationReader;
        private readonly IList<string> processedFiles;

        private event EventHandler<string> Printed;

        public PrintService(IConfigurationReader configurationReader)
        {
            this.configurationReader = configurationReader;
            this.processedFiles = new List<string>();
        }

        public void PrintNewEntry(string filePath)
        {
            string fileName = Path.GetFileName(filePath);
            if (processedFiles.Contains(fileName))
            {
                return;
            }

            while (true)
            {
                try
                {
                    // [ HACK ] This will throw an error if the file is still being written to
                    FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None);
                    stream.Close();
                    // [ END ]

                    PrintDialog printDialog = new PrintDialog();
                    StreamReader reader = new StreamReader(filePath);
                    Paragraph receipt = new Paragraph(new Run(reader.ReadToEnd()))
                    {
                        Margin = new Thickness { Right = configurationReader.ConfigurationModel.Margin,
                            Left = configurationReader.ConfigurationModel.Margin },
                        FontFamily = new FontFamily(configurationReader.ConfigurationModel.FontFamily),
                        FontSize = configurationReader.ConfigurationModel.FontSize
                    };

                    FlowDocument document = new FlowDocument(receipt)
                    {
                        FontSize = configurationReader.ConfigurationModel.FontSize,
                        FontFamily = new FontFamily(configurationReader.ConfigurationModel.FontFamily),
                    };

                    if (reader != null)
                        reader.Close();

                    IDocumentPaginatorSource paginatorSource = document;
                    DocumentPaginator paginator = paginatorSource.DocumentPaginator;
                    var docSize = new Size { Width = 96 * 3.14961, Height = 96 * 11 };
                    paginator.PageSize = docSize;
                    printDialog.PrintDocument(paginator, "TicketPrint");

                    string copyPath = configurationReader.GetPrintFolder();
                    Guid guid = Guid.NewGuid();
                    File.Copy(filePath, $@"{copyPath}\{guid.ToString()}.txt");

                    processedFiles.Add(fileName);
                    Console.WriteLine("File Read!");
                }
                catch (Exception e)
                {
                    Console.WriteLine("An Exception Occurred");
                    continue;
                }

                break;
            }

            Printed?.Invoke(this, filePath);
        }

        public void SubscribeToPrinted(EventHandler<string> handler)
        {
            Printed += handler;
        }
    }
}
