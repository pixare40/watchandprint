﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public interface IPrintService
    {
        void PrintNewEntry(string fileName);

        void SubscribeToPrinted(EventHandler<string> handler);
    }
}
