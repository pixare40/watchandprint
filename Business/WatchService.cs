﻿using System;
using System.IO;

namespace BusinessLayer
{
    public class WatchService : IWatchService
    {
        private readonly IPrintService printService;
        private readonly IConfigurationReader configurationReader;

        public WatchService(IPrintService printService, IConfigurationReader configurationReader)
        {
            this.printService = printService;
            this.configurationReader = configurationReader;
        }

        public void StartFileSystemWatch()
        {
            FileSystemWatcher fileSystemWatcher = new FileSystemWatcher();
            fileSystemWatcher.Path = configurationReader.GetWatchFolder();
            fileSystemWatcher.NotifyFilter = NotifyFilters.LastWrite;
            fileSystemWatcher.Filter = "*.txt";

            fileSystemWatcher.Changed += new FileSystemEventHandler(OnChange);
            fileSystemWatcher.EnableRaisingEvents = true;
        }

        private void OnChange(object sender, FileSystemEventArgs e)
        {
            string fileName = e.FullPath;

            printService.PrintNewEntry(fileName);
        }
    }
}
