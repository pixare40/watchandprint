﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public interface IConfigurationReader
    {
        ConfigurationModel ConfigurationModel { get; set; }
        
        string GetWatchFolder();

        string GetPrintFolder();
    }
}
