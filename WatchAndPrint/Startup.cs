﻿using BusinessLayer;
using System;

namespace WatchAndPrint
{
    class Startup
    {
        [STAThread]
        static void Main()
        {
            DIContainer container = new DIContainer();
            DIContainer.SetupContainer();

            App app = new App();
            app.MainWindow = new MainWindow();

            app.MainWindow.Show();
            app.Run();
        }
    }
}
