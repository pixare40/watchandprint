﻿using System;
using System.Windows.Input;

namespace WatchAndPrint
{
    public class DelegateCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return _canexecute == null ? true : _canexecute(parameter);
        }

        public void Execute(object parameter)
        {
            _executeAction(parameter);
        }

        private readonly Action<object> _executeAction;
        private readonly Predicate<object> _canexecute;

        public DelegateCommand(Action<object> executeAction)
        {
            this._executeAction = executeAction;
        }

        public DelegateCommand(Action<object> executeaction, Predicate<object> func)
            : this(executeaction)
        {
            this._canexecute = func;
        }

        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, EventArgs.Empty);
            }
        }
    }
}
