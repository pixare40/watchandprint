﻿using BusinessLayer;
using System;
using System.Collections.ObjectModel;

namespace WatchAndPrint
{
    public class MainWindowVM : ViewModelBase
    {
        private readonly ObservableCollection<string> printedFiles;

        public IPrintService PrintService { get; set; }

        public IWatchService WatchService { get; set; }

        public ObservableCollection<string> PrintedFiles
        {
            get { return printedFiles; }
        }

        public MainWindowVM()
        {
            printedFiles = new ObservableCollection<string>();
            PrintService = DIContainer.container.GetInstance<IPrintService>();
            WatchService = DIContainer.container.GetInstance<IWatchService>();

            WatchService.StartFileSystemWatch();
            PrintService.SubscribeToPrinted(OnPrinted);
        }

        private void OnPrinted(object sender, string printedFile)
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                PrintedFiles.Add(printedFile);
            });
        }
    }
}
