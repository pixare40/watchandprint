﻿using BusinessLayer;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WatchAndPrint
{
    public class DIContainer
    {
        public static readonly Container container;

        static DIContainer()
        {
            container = new Container();
        }

        public static void SetupContainer()
        {
            container.RegisterSingleton<IWatchService, WatchService>();
            container.RegisterSingleton<IPrintService, PrintService>();
            container.RegisterSingleton<IConfigurationReader, ConfigurationReader>();
        }
    }
}
